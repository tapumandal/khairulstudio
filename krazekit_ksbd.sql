-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 29, 2017 at 02:34 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `krazekit_ksbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Tapu Mandal', 'online.tapu@gmail.com', '$2y$10$xtTJXNfVLnqITgbRogjVOuRJX7uj1ZnlvCsH8QnIIOhcoQd2ySjsq', 'GkKgRtBpVFSPtr8BTq9QuH4ssY3icgIHEb1QQ3IVoDcIab4XbFlevs2zZjcp', '2017-10-21 00:46:03', '2017-10-21 00:46:03', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_date` date NOT NULL,
  `event_code` text COLLATE utf8mb4_unicode_ci,
  `event_note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_photos`
--

CREATE TABLE IF NOT EXISTS `event_photos` (
  `id` int(10) unsigned NOT NULL,
  `img_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallarys`
--

CREATE TABLE IF NOT EXISTS `gallarys` (
  `id` int(10) unsigned NOT NULL,
  `img_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallarys`
--

INSERT INTO `gallarys` (`id`, `img_name`, `type`, `created_at`, `updated_at`) VALUES
(10, 'lifestyle10011159175137.jpg', 'lifestyle', '2017-11-17 07:59:51', '2017-11-17 07:59:51'),
(11, 'lifestyle161211117285.jpg', 'lifestyle', '2017-11-17 08:01:02', '2017-11-17 08:01:02'),
(12, 'lifestyle1562111173264.jpg', 'lifestyle', '2017-11-17 08:01:32', '2017-11-17 08:01:32'),
(13, 'lifestyle1802112173159.jpg', 'lifestyle', '2017-11-17 08:02:31', '2017-11-17 08:02:31'),
(15, 'lifestyle1812114172377.jpg', 'lifestyle', '2017-11-17 08:04:23', '2017-11-17 08:04:23'),
(16, 'lifestyle1312115171012.jpg', 'lifestyle', '2017-11-17 08:05:10', '2017-11-17 08:05:10'),
(17, 'lifestyle138211617708.jpg', 'lifestyle', '2017-11-17 08:06:07', '2017-11-17 08:06:07'),
(18, 'lifestyle1942118171436.jpg', 'lifestyle', '2017-11-17 08:08:14', '2017-11-17 08:08:14');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2017_10_22_174902_gallary_table_creat', 2),
(6, '2017_10_22_174956_event_photos_table_creat', 2),
(7, '2017_10_22_175038_userinfos_table_creat', 2),
(8, '2017_10_22_175054_motions_table_creat', 2),
(9, '2017_10_22_175107_shops_table_creat', 2),
(11, '2017_10_22_221547_change_userinfos_event_date_type', 3),
(12, '2017_10_22_223701_add_event_date_to_userinfos', 4),
(13, '2017_10_23_000755_add_column_userinfos', 5),
(14, '2017_10_23_051222_change_userinfos_table_name', 6),
(15, '2017_10_23_163915_add_event_type_events', 7);

-- --------------------------------------------------------

--
-- Table structure for table `motions`
--

CREATE TABLE IF NOT EXISTS `motions` (
  `id` int(10) unsigned NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('online.tapu@gmail.com', '$2y$10$cPe34cjxMpYtW3KfzuCjl.Sl5cz4cPW6BtALFFJQ.yk.DpaX2Geea', '2017-11-07 01:41:04');

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `webaddress` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promoid` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `des` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `created_at`, `updated_at`, `webaddress`, `campaign`, `promoid`, `name`, `des`, `img`) VALUES
(7, '2017-11-26 00:36:11', NULL, 'http://teespring.com/', 'captive-owl/', '?pr=ksc018', 'Captive Owl - Limited Edition!', 'Internet Exclusive! - Available for a few days only.  Choose your style and color below 100% Satisfaction Guaranteed Safe & Secure Checkout', '0captive-owl.jpg'),
(9, '2017-12-08 19:06:25', NULL, 'http://teespring.com/', 'happy-hanukkah-limited-editi/', '?pr=KSC018', 'Happy Hanukkah - New Arrival!', 'Internet Exclusive! - Available for a few days only.  Choose your style and color below 100% Satisfaction Guaranteed Safe & Secure Checkout', '0happy-hanukkah-limited-editi.jpg'),
(10, '2017-12-19 18:46:57', NULL, 'http://teespring.com/', 'happy-boxing-2017-for-b-9069/', '?pr=KSC018', 'Happy Boxing 2017 - FOR BOXING DAY!', 'Limited Edition! Internet Exclusive - Available for a few days only.  Find your size + Choose your style and color below 100% Satisfaction Guaranteed Safe & Secure Checkout', '0happy-boxing-2017-for-b-9069.jpg'),
(11, '2017-12-29 09:04:00', NULL, 'http://teespring.com/', 'pinion-pillow/', '?pr=KSC018', 'Pinion Pillow', 'Choose your style and color below 100%  Satisfaction Guaranteed Safe & Secure Checkout', '0pinion-pillow.jpg'),
(12, '2017-12-29 12:32:12', NULL, 'http://teespring.com/', 'cow-skull-2018-edition-basic/', '?pr=KSC018', 'Cow Skull 2018 Edition', 'Limited Edition! Internet Exclusive - Available for a few days only.  Find your size + Choose your style and color below 100% Satisfaction Guaranteed Safe & Secure Checkout', '0cow-skull-2018-edition-basic.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_photos`
--
ALTER TABLE `event_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallarys`
--
ALTER TABLE `gallarys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motions`
--
ALTER TABLE `motions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_photos`
--
ALTER TABLE `event_photos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallarys`
--
ALTER TABLE `gallarys`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `motions`
--
ALTER TABLE `motions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
