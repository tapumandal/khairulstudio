<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use \Input as Input;	
use Illuminate\Support\Facades\DB;
use File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Crypt;
use ZipArchive;
use App\Client;
use App\Event;


class UserAction extends Controller
{	
	public function allEvents(Request $request){

		$clients = Client::where('email', $request->email)->first();

		if($clients->email == $request->email){
			if(Crypt::decryptString($clients->sign_id) == $request->sign_id){


				$events = Event::get();
				$eventList = array();
				foreach ($events as $event) {
					if(Crypt::decryptString($event->sign_id) == $request->sign_id){
						// array_push($eventList, Crypt::decryptString($event->event_code));
						array_push($eventList, $event);

					}
				}

				// echo "<pre>";
				// print_r($eventList);
				// die();
				return view('events_photo', ['eventList' => $eventList]);
			}
		}
    }



    public function download(Request $request){

    	echo "Your photos is being ready to Download<br>";

    	$images = DB::table('event_photos')->get()->where('event_code', '=', Crypt::decryptString($request->event_code));



		$zip = new ZipArchive;	
		$zip_name = "123456.zip"; // Zip name


		$zip->open($zip_name, ZipArchive::CREATE);

		foreach ($images as $image) {
			// if (Hash::check(Crypt::decryptString($request->event_code), $images->)) {
				$zip->addFile('images/users/'.$image->img_name);
			// }
		  		

		}

		$zip->close();

		if(file_exists($zip_name)){
			header('Content-type: application/zip');
			header('Content-Desposition: attachment; filename="'.$zip_name.'"');
			

			readfile($zip_name);

			unlink($zip_name);

		}else{
		}

		return redirected()->back();
    }
}
