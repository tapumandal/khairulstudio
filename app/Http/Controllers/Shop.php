<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use \Input as Input;
use Illuminate\Support\Facades\DB;
use File;
use Illuminate\Support\Facades\Redirect;


class Shop extends Controller
{
    

    public function publicShow(){
        
        $products = DB::table('shops')->orderBy('id', 'DESC')->get();
        
        return view('shop', compact('products'));
    }


    public function show(){
	    
	    $products = DB::table('shops')->orderBy('id', 'DESC')->get();
    	
    	return view('admin.shop', compact('products'));
    }


    public function upload(){
    	

    	$img = Input::file('images');
    	$webaddress = Input::input('webaddress');
    	$campaign = Input::input('campaign');
    	$promoId = Input::input('promoid');
    	$name = Input::input('name');
    	$des = Input::input('des');

    	foreach ($img as $key => $value) {
    		$date =new \DateTime();

    		$imgName = ImageProcessing::moveImage($value, 'images/shop/', $key.$campaign[$key]);
            if(empty($des[$key])){
                $des[$key] = "";
            }

            if(empty($promoId[$key])){
                $promoId[$key] = "";
            }

    		DB::table('shops')->insert(['webaddress'=>$webaddress, 'campaign'=>$campaign[$key].'/', 'promoid'=>'?pr='.$promoId[$key],  'name'=>$name[$key], 'des'=>$des[$key], 'img'=>$imgName, 'created_at'=>$date]);


    	}

    	return redirect('shopshow')->with('status', 'Product Updated');
    }	


    public function remove(){
    	$campaign = Input::input('campaign');
        DB::table('shops')->where('campaign', '=', $campaign)->delete();
        
        return redirect('shopshow');
    }

}

