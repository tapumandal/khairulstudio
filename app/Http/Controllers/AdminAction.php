<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Input as Input;

use View;
use App\Event;
use App\Gallary;
use Mail;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use File;
use Illuminate\Support\Facades\Redirect;

class AdminAction extends Controller
{
    public function insertHomeSliderImg(){

    	$actionStatus ="<br>";
    	for ($i=1; $i < 5; $i++) { 
		    if(Input::hasFile('up_img_'.$i)){
		    	$file = Input::file('up_img_'.$i);
		    	$file->move('images/public/', 'sh'.$i.'.jpg');
		    	$actionStatus = $actionStatus."".$i." No. Image Uploaded Successfully.<br>";

		    }else{
		    	$actionStatus = $actionStatus."".$i." No. Image Not Uploaded.<br>";
		    }
		}

		return redirect('slider')->with('actionStatus', $actionStatus);
    }

    public function insertShortcutImg(){
		$actionStatus ="<br>";
    	for ($i=1; $i < 5; $i++) { 
		    if(Input::hasFile('up_img_'.$i)){
		    	$file = Input::file('up_img_'.$i);
		    	$file->move('images/public/', 'sci'.$i.'.jpg');
		    	$actionStatus = $actionStatus."".$i." No. Image Uploaded Successfully.<br>";

		    }else{
		    	$actionStatus = $actionStatus."".$i." No. Image Not Uploaded.<br>";
		    }
		}

		return redirect('shortcut')->with('actionStatus', $actionStatus);    	
    }

    public function insertGallaryImg(Request $request ){
    	$cata = Input::input('catagory');

    	$images = Input::file('images');
    	// $images = $request->file('images');
    	$i=1;
    	if($request->hasFile('images')){
    		foreach ($images as $image) {
    			$imgName = ImageProcessing::moveImage($image, 'images/gallary/', Input::input('catagory').$i.AdminUserTask::eventImgNameGenerate());
    			$i++;

    			$gallaryImgUp = new Gallary();
    			$gallaryImgUp->img_name = $imgName;
    			$gallaryImgUp->type = Input::input('catagory');
    			$gallaryImgUp->save();
    			
    		}

    	}


    	return view('admin.gallary', ['status' => 'Image Uploaded Successfully']);
    }


    public function allImgAction(){

        $event_code =  Input::input('event_code');

        $imgList = DB::table('event_photos')->get()->where('event_code', $event_code);

        $type = 'event';
        $cata = "";
        return view('admin.gridLayout', compact('cata', 'event_code', 'type', 'imgList'));
    }

    public function allImgAction2($cata){
        $imgList = DB::table('gallarys')->get()->where('type', $cata);

        $type = "gallary";
        $event_code = "";
        return view('admin.gridLayout', compact('cata', 'event_code', 'type', 'imgList'));   
    }
    


    public function deleteImage(){
        



        $imgList = Input::input('images');

        foreach ($imgList as $img => $value) {


            if(Input::input('type')=='gallary'){
                File::delete('images/gallary/'.$value);
                DB::table('gallarys')->where('img_name', '=', $value)->delete();

                // return AdminAction::allImgAction2(Input::input('cata'));

            }else{
                File::delete('images/users/'.$value);
                DB::table('event_photos')->where('img_name', '=', $value)->delete();
                
                // return AdminAction::allImgAction();
            }
        }

        if(Input::input('type')=='gallary'){
            return AdminAction::allImgAction2(Input::input('cata'));
        }else{
            return AdminAction::allImgAction(); 
        }
        
    }




    
}
