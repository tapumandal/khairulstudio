<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use \Input as Input;
use Illuminate\Support\Facades\DB;
use File;
use Illuminate\Support\Facades\Redirect;
class Motion extends Controller
{
    
    public function publicMotion(){

        $motionLink = DB::table('motions')->orderBy('id', 'DESC')->get();   
        return view('motion', compact('motionLink'));

    }


    public function showMotion(){

    	$motionLink = DB::table('motions')->orderBy('id', 'DESC')->get();	
        return view('admin.motion_upload', compact('motionLink'));

    }



    public function uploadMotion(){

        $sites = Input::input('site');
        $Links = Input::input('motionLink');
        $Title = Input::input('linkTitle');


        
        foreach ($Links as $key => $link) {
        	if(strlen($link) > 3){
	        	$date =new \DateTime();
	            DB::table('motions')->insert(['site'=>$sites[$key], 'link'=>$link, 'title'=>$Title[$key], 'created_at'=>$date]);
        	}
        }

        return redirect('motionshow');
    }

    public function deleteMotion(){


    	$link = Input::input('videoLink');
    	DB::table('motions')->where('link', '=', $link)->delete();
        
        return redirect('motionshow');
    }

}