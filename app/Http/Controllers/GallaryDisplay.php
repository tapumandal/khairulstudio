<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use View;

class GallaryDisplay extends Controller
{
    public function gallaryShow(Request $request){
    	$type =  $request->path();

    	$imageList = DB::table('gallarys')->get()->where('type', $type);

    	return view('store', compact('type', 'imageList'));
    	
    }
}
