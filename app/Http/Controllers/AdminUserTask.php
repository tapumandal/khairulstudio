<?php

namespace App\Http\Controllers;

use App\Client;
use App\Event;
// use App\Http\Controllers\Input;
use App\event_photo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Mail;
use View;
use \Input as Input;

class AdminUserTask extends Controller
{

    public function createEvent(Request $request)
    {


        $current_time = Carbon::now()->toDateTimeString();

        $event              = new Event();
        $event->sign_id     = $request->sign_id;
        $event->event_title = $request->event_title;
        $event->event_code  = $this->eventCode($current_time, 6);
        $event->event_date  = $request->event_date;
        $event->event_type  = $request->event_type;
        $event->event_note  = $request->event_note;
        $event->save();

        $status = "New Event Created Successfully.";

        return redirect('event/'.$request->clientid.'')->with('actionStatus', $status);
        // Event::create(Request::all());
    }

    public function createClient(Request $request)
    {

        $client           = new Client();
        $client->name     = $request->client_name;
        $client->email    = $request->email;
        $client->phone    = $request->phone;
        $client->location = $request->location;
        $client->sign_id  = $this->eventCode($request->client_name, 8);
        $client->save();

        $status = "New Clentt Added Successfully.";
        return redirect('client')->with('actionStatus', $status);
    }

    protected function eventCode($client, $len)
    {

        $eventCode = '';

        $fLetter = $client[0];
        $lLetter = $client[strlen($client) - 1];

        $characters       = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);

        $randomString = '';
        for ($i = 0; $i < $len - 2; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];

            if ($i == 0) {
                $randomString .= strtoupper($fLetter);
            } else if ($i == 2) {
                $randomString .= strtoupper($lLetter);
            }
        }

        // $encrypted = Crypt::encryptString('Hello world.');
        // $decrypted = Crypt::decryptString($encrypted);

        $eventCode = Crypt::encryptString($randomString);

        return $eventCode;
    }

    public function email()
    {

        $data = array('name' => "Khairul Ahmed");

        Mail::send(['text' => 'admin.emailsend'], $data, function ($message) {
            $message->to('online.tapu@gmail.com', 'Tapu Mandal')->subject('Laravel Basic Testing Mail');
            $message->from('khairul@khairulstudio.com', 'khairulstudio');
        });

    }

    public function showClient()
    {

        $client = Client::all()->sortByDesc('created_at');

        foreach ($client as $e) {
            $e->sign_id = Crypt::decryptString($e->sign_id);
        }

        return view('admin.client', ['clients' => $client]);
    }

    public function showEvent(Request $request)
    {

        $clientid = $request->clientid;

        $clientInfo = Client::get()->where('id', '=', $clientid)->first();

        $sign_id = $clientInfo->sign_id;

        $event = Event::get()->where('sign_id', '=', $sign_id)->where('is_active', '=', '1')->sortByDesc('created_at');

        foreach ($event as $e) {
            $e->event_code = Crypt::decryptString($e->event_code);
        }

        return view('admin.event', ['events' => $event, 'sign_id' => $sign_id, 'clientid' => $clientid]);
    }

    public function imgselect()
    {
        return view('admin.uploadeventimg', ['event_code' => Input::input('event_code'), 'name' => Input::input('name')]);
    }

    public function imgUpload(Request $request)
    {

        $eventCode  = Input::input('event_code');
        $clientName = Input::input('name');

        // if(Input::hasfile('images')){
        //     $images = Input::input('images');

        // }

        $files = $request->file('images');
        $i     = 1;
        if ($request->hasFile('images')) {
            foreach ($files as $file) {

                // $file->store('images/users/');
                $imgName = ImageProcessing::moveImage($file, 'images/users/', $i . AdminUserTask::eventImgNameGenerate());
                $i++;

                $upPhotos             = new event_photo();
                $upPhotos->img_name   = $imgName;
                $upPhotos->event_code = $eventCode;
                $upPhotos->save();
            }

            return view('admin.uploadeventimg', ['status' => 'Image Uploaded Successfully', 'name' => $clientName, 'event_code' => $eventCode]);
        }

    }

    public static function eventImgNameGenerate()
    {

        $mytime = Carbon::now();

        $characters       = '0123456789';
        $charactersLength = strlen($characters);

        $randomString = '';
        for ($i = 0; $i < 4; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $name = $randomString[0] . $randomString[1] . $mytime->hour . $mytime->month . $mytime->minute . $mytime->day . $mytime->second . $randomString[2] . $randomString[3];

        return $name;
    }

    public function deactivateEvent(Request $request){

        $eventInfo = Event::get()->where('is_active', '=', '1');

        foreach ($eventInfo as $event) {
            if(Crypt::decryptString($event->event_code) == $request->event_code){
                // echo "Match<br>";
                // echo $event->id;
                $update = Event::where('id', $event->id)->update(['is_active' => '0']);
            }
        }

        $status = "Event Deactivated Successfully.";
        return redirect()->back()->with('actionStatus', $status);
        
    }
}
