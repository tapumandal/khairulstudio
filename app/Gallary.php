<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallary extends Model
{
    //
    protected $table = 'gallarys';
    protected $fillable=array('img_name', 'type');
}
