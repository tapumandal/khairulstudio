<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  
    // protected $fillable = ['name', 'email', 'phone', 'event_title', 'event_date', 'event_type', 'event_note'];

    protected $fillable = array( 'sign_id', 'event_title', 'event_code', 'event_date', 'event_type', 'event_note', 'is_active');
}
