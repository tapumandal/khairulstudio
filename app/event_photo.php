<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event_photo extends Model
{
    protected $fillable =array('img_name', 'event_code');
}
