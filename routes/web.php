<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('home', 'HomeController@index');

Route::get('store', function () {
    return view('store');
});

Route::get('fashion', 'GallaryDisplay@gallaryShow');
Route::get('lifestyle', 'GallaryDisplay@gallaryShow');
Route::get('wedding', 'GallaryDisplay@gallaryShow');
Route::get('familyportrait', 'GallaryDisplay@gallaryShow');
Route::get('studio', 'GallaryDisplay@gallaryShow');



Route::get('shop', 'Shop@publicShow');
Route::get('motion', 'Motion@publicMotion');



Route::get('about', function () {
    return view('about');
});


Route::get('contact', function () {
    return view('contact');
});

Route::get('signin', function () {
    return view('login');
});
	
Route::get('download', function(){
	return view('download');
});



Route::post('allevents', 'UserAction@allEvents');
Route::post('downloading', 'UserAction@download');

// Route::post('khairulstudio_photos', 'UserAction@download');


Route::get('auth/logout', 'Auth\AuthController@logout');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');



Route::group(['middleware' => ['auth']], function(){
	
	Route::get('admin', function(){
		return view('admin.admin');
	});

	Route::get('client', 'AdminUserTask@showClient');
	
	// Route::post('event', 'AdminUserTask@showEvent');
	Route::get('event/{clientid?}', 'AdminUserTask@showEvent');

	Route::get('gallary', function(){
		return view('admin.gallary');
	});

	Route::get('slider', function(){
		return view('admin.slider');
	});

	Route::get('shortcut', function(){
		return view('admin.shortcut');
	});

	Route::get('motionshow', 'Motion@showMotion');
	Route::post('upmotion', 'Motion@uploadMotion');
	Route::post('demotion', 'Motion@deleteMotion');

	Route::get('shopshow', 'Shop@show');
	Route::post('shopup', 'Shop@upload');
	Route::post('shopremove', 'Shop@remove');



	Route::get('uploadeventimg', function(){
		return view('admin.uploadeventimg');
	});

	Route::post('sliderimg', 'AdminAction@insertHomeSliderImg');
	Route::post('shortcutimg', 'AdminAction@insertShortcutImg');

	Route::post('createevent/{clientid?}', 'AdminUserTask@createEvent');
	Route::post('deactivate', 'AdminUserTask@deactivateEvent');
	Route::post('createclient', 'AdminUserTask@createClient');
	
	Route::post('email', 'AdminUserTask@email');
	// Route::post('imgupload', 'AdminUserTask@imgUpload');
	
	Route::post('imgselect', 'AdminUserTask@imgselect');

	Route::post('imgupload', 'AdminUserTask@imgUpload');


	
	Route::post('uploadgallaryimg', 'AdminAction@insertGallaryImg');


	Route::post('display', 'AdminAction@allImgAction');
	Route::get('display/{type}', ['uses' => 'AdminAction@allImgAction2']);	

	
	
	Route::post('imgdelete', 'AdminAction@deleteImage');

	
});