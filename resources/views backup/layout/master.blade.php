<!DOCTYPE html>
<html>
<head>
	<!-- <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script> -->


	<!-- <link rel="stylesheet" href="{{ URL::asset('css/cover.css') }}" /> -->

	

	<title>@yield('title')</title>
	<link rel="icon" src="{{ URL::asset ('images/public/tab_logo.png') }}">
	<link rel="shortcut icon" href="http://www.khairulstudio.com/images/icon/tab_l.ico" type="image/ico" />
</head>
<body>
	@section('navigation_bar')
	@show

	<div class="">
		@yield('main_content')
	</div>


	<div class="footer">
		@yield('footer')
	</div>
</body>
</html>