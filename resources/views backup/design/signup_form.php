<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
<div class="col-sm-12 signup">
	

	<div class="signup_field">
		<div class="signup_field_inside col-sm-12">
			<h1>Create your Account</h1>
			<form action="" method="post">
				<div class="col-sm-6 signup_left">
					<span>Name</span>
					<input type="text" name="fname" placeholder="First">
					<input type="text" name="sname" placeholder="Second">	

					<span>Choose your username</span>
					<input type="text" name="username">	

					<span>Create a password</span>
					<input type="text" name="password">	

					<span>Confirm your password</span>
					<input type="text" name="password2">	

					
				</div>
				
				<div class="col-sm-6 signup_right">
					<span>Birthday</span>
					<p><input type="text" id="datepicker" name="date" placeholder="Date"></p>

					<span>Gender</span>
					<select name="gender">
						<option>I am...</option>
						<option value="male">Male</option>
						<option value="female">Female</option>
						<option value="others">others</option>
					</select>
					<br>
					<span>Location</span>
					<select name="location">
						<option value="bangladesh">Bangladesh</option>
					</select>

					<span>Your current address</span>
					<input type="text" name="address">

					<input type="submit" value="Create">
				</div>
			</form>
				
		</div>
	</div>
</div>