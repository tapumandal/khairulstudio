@include ('include.header')
@include ('design.overlay_nav')
<div class="col-sm-12   cover " >
	<div class="col-sm-12 cover_inside">
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
			<div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" onclick="showNav()">
		        <span class="icon-bar"></span>
		        <!-- <span class="icon-bar"></span> -->
		        <span class="icon-bar"></span>                        
		      </button>
		    </div>
		  </div>
		</nav>
			
		<div class="logo">
			<a href="./"> <img class="img-responsive" src="{{ URL::asset('images/default/logo_khairulstudio.png') }}" alt="Khairul Studio Logo"> </a>
		</div>



	



		<div class="header_btn">



			

			<div class="menu_btn">
				<div class="home_option">
					<a href=""> <img class="img-responsive" src="{{ URL::asset('images/default/menu-icon2.png') }}"> </a>

					<div class="dropdown">
					  <!-- <button class="dropbtn"></button> -->
					  <div class="dropdown-content">
					  
					  	<div class="arrow-down"></div>
					  	<div class="drop_menu_top">
					  		<span>Your photo is missing</span>
					  	</div>
					  	<div class="drop_down_underline"></div>
					    <a href="./store"><img class="img-responsive" src="{{ URL::asset('images/icon/photo.png') }}"> Store</a>
					    <a href="./download"><img class="img-responsive" src="{{ URL::asset('images/icon/download.png') }}"> Download</a>
					    <a href="./contact"><img class="img-responsive" src="{{ URL::asset('images/icon/contact.png') }}">Contact</a>
    					<a href="./about"><img class="img-responsive" src="{{ URL::asset('images/icon/about.png') }}">About</a>
		                  @if(Auth::check())
	                            <a href="{{ route('logout') }}"
	                                onclick="event.preventDefault();
	                                         document.getElementById('logout-form').submit();">
	                                Logout
	                            </a>

	                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                {{ csrf_field() }}
	                            </form>
	                        @else
	                        <a href="{{ route('login') }}">Login</a>
	                        <a href="{{ route('register') }}">Register</a>
	                      @endif
	                      
					  </div>
					</div>
				</div>
			</div>

			<div class="wide_screen_nav">
				@include ('design.navbar')
			</div>
		</div>
		
	</div>
</div>

	
	