<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
	$( function() {
    	$( "#datepicker" ).datepicker();
  	} );
</script>

<div class="download_p">
	<div class="download_h">
		<div class="d_h_1">
			<h3>DOWNLOAD PHOTOS</h3>	
		</div>

		<div class="d_h_2">
			<h3>KHAIRUL STUDIO Photo Downloads</h3>	
		</div>

	</div>

	<div class="download_panel">
		<form action="#" method="POST">
			<div class="form_top">
				<h4>Manually find photos for my KHAIRUL STUDIO photos</h4>
			</div>

			<div class="download_form">
				<div class="input1">
					<div class="input_txt"><h4>Photos & Video Type: </h4>	</div>
					
					<div class="input_box">
						<select name="photo_type">
							<option value=""></option>
							<option value="life_style">Life Style</option>
							<option value="wedding">Wedding</option>
						</select>
					</div>
				</div>

				<div class="input2">
					<div class="input_txt"><h4>Date of Shoot: </h4></div>
					<div class="input_box">
						<input type="text" id="datepicker"></p>
					</div>
				</div>

				<div class="input3">
					<div class="input_txt"><h4>Location: </h4></div>

					<div class="input_box">
						<select name="location">
							<option value=""></option>
							<option value="Dhanmondi">Dhanmondi</option>
							<option value="Gulshan">Gulshan</option>
						</select>
					</div>
				</div>

				<div class="input4">
					<div class="input_txt"><h4>Sign Id: </h4></div>
					<div class="input_box">
						<input type="text" name="sing_id"></p>
						<input type="submit" value="SEARCH">
					</div>


				</div>

			</div>

			
		</form>

		<div class="share_btn">
			<h4>Like to the latest news on the KHAIRUL STUDIO page!<br>
				Connect with us online : <a href=""><i class="fa fa-facebook-square"></i></a>  <i class="fa fa-instagram"></i> <i class="fa fa-flickr"></i>
			</h4>
		</div>
	</div>
</div>