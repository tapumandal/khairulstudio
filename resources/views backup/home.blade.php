<?php
	// include 'include/header.php';
	// echo "<div class='top_display'>";
	// include 'view/design/cover.php';
	// include 'view/design/slider.php';
	// echo "</div>";
	// include 'include/footer.php';
?>


@extends('layout.master')

@section('title', 'Khairul Studio')

@section('navigation_bar')
	@include('design.cover')
@endsection

@section('main_content')
		@include('design.home_slider')

		@include('design.home_box')
@endsection