
@extends('layouts.master')

@section('title', 'Contact')

@section('navigation_bar')
	@include('include.cover')
@endsection

@section('main_content')
	<div class="col-md-12 contact">
		<div class="contact_inside">
			<div class="contact_text">
				<h2>CONTACT</h2>
				<span>Phone: +880 1783 611431 | Email: info@khairulstudio.com</span>
				<span>Green Park 548, West Shewrapara Shamim Sharani Mirpur, Dhaka, Bangladesh</span>
			</div>
			<?php //echo $mailResult; ?>
			<div class="contact_form">
				<form id="html5Form" action="<?php echo $_SERVER['REQUEST_URI']; ?>&action=send" method="POST">
					<input type="text" name="name" placeholder="your name" maxlength="30" minlength="4">
					<input type="email" name="email" placeholder="your mail">
					<textarea name="comment" placeholder="message" maxlength="200" minlength="10"></textarea>

					<input type="submit" value="SEND">
				</form>
			</div>
		</div>
	</div>
@endsection