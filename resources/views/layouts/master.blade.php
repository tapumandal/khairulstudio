<!DOCTYPE html>
<html>
<head>
	<!-- <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script> -->
	<!-- <link rel="stylesheet" href="{{ URL::asset('css/cover.css') }}" /> -->
<title>@yield('title')</title>


	<link rel="shortcut icon" href="http://www.khairulstudio.com/images/icon/tab_l.ico" type="image/ico" />
<!-- bootstrap -->
	<!-- <script src="bootstrap/js/bootstrap.min.js"></script> -->
	<link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet" />
	<script src="{{ URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js') }}"></script>
	<!-- bootstrap -->

	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Palanquin:100|Source+Sans+Pro:300" rel="stylesheet">
	<!-- for nav bar -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Page Lodder -->
    	

	    <!-- <style type="text/css">
			.page_loader {
				position: fixed;
				left: 0px;
				top: 0px;
				width: 100%;
				height: 100%;
				background: white;

				z-index: 9999;

			}

			.loader {
			  border: 16px solid #f3f3f3;
			  border-radius: 50%;
			  border-top: 16px solid #3498db;
			  width: 120px;
			  height: 120px;


			  -webkit-animation: spin 2s linear infinite; 
			  animation: spin 2s linear infinite;
			}

			@-webkit-keyframes spin {
			  0% { -webkit-transform: rotate(0deg); }
			  100% { -webkit-transform: rotate(360deg); }
			}

			@keyframes spin {
			  0% { transform: rotate(0deg); }
			  100% { transform: rotate(360deg); }
			}
		</style>

		<script type="text/javascript">
		$(document).ready(function(){
			var WHeight = $(window).height();
			var WWidth = $(window).width();
			
			var loaderWidth = (WWidth/2)-60;
			var loaderHeight = (WHeight/2)-60;

			$(".loader").css("cssText", "margin-left:"+loaderWidth+"px; margin-top:"+loaderHeight+"px;");

			$(window).load(function() {
				$(".page_loader").fadeOut("slow");
				$(".loader").fadeOut("slow");
			})

			setTimeout(function(){ 
				$(".page_loader").fadeOut("slow");
				$(".loader").fadeOut("slow");
			}, 5000);

		});

		

		</script> -->

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		
    <!-- Page Lodder -->



    <link rel="stylesheet" href="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') }}">
    <link href="{{ URL::asset('css/navbar.css') }}" rel="stylesheet" />
    <script src="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') }}"></script>
    <!-- for nav bar -->

	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('css/bootstrapModified.css') }}" rel="stylesheet" />


	<script src="{{ URL::asset('js/self.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/slider.js') }}"></script>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->


	<link rel="icon" src="{{ URL::asset ('images/public/tab_logo.png') }}">
</head>
<body>
	<div class="page_loader">
		<div class="loader"></div>
	</div>

	@section('navigation_bar')
	@show

	<div class="mainpanel">
		@yield('main_content')
	</div>


	<div class="footer">
		<!-- @yield('footer') -->
		@include('include.footer')
	</div>
</body>
</html>