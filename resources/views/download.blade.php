
@extends('layouts.master')

@section('title', 'Download')

@section('navigation_bar')
	@include('include.cover')
@endsection

@section('main_content')
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script type="text/javascript">
		$( function() {
	    	$( "#datepicker" ).datepicker();
	  	} );
	</script>
	<div class="total_download_p download">
		<div class="download_p">
			
			<div class="download_h">
				<div class="d_h_1">
					<h1>DOWNLOAD</h1>	
				</div>

				<div class="d_h_2">
					<h3>Find your photos</h3>	
				</div>

			</div>

			<div class="download_panel">
				<form action="allevents" method="POST">
					

					<div class="download_form">
						{{-- <div class="input1">
							<div class="input_txt"><h4>Photos & Video Type: </h4>	</div>
							
							<div class="input_box">
								<select name="photo_type">
									<option value=""></option>
									<option value="life_style">Life Style</option>
									<option value="wedding">Wedding</option>
									<option value="family_portrait">Family Portrait</option>
									<option value="studio">Studio</option>
								</select>
							</div>
						</div>

						<div class="input2">
							<div class="input_txt"><h4>Date of Shoot: </h4></div>
							<div class="input_box">
								<input type="text" id="datepicker"></p>
							</div>
						</div> --}}

						<!-- <div class="input3">
							<div class="input_txt"><h4>Location: </h4></div>

							<div class="input_box">
								<select name="location">
									<option value=""></option>
									<option value="Dhanmondi">Dhanmondi</option>
									<option value="Gulshan">Gulshan</option>
								</select>
							</div>
						</div> -->
						<div class="input4">
							<div class="input_txt"><h4>Email: </h4></div>
							<div class="input_box">
								<input type="email" name="email" required="email">
							</div>
						</div>

						<div class="input4">
							<div class="input_txt"><h4>Sign Id: </h4></div>
							<div class="input_box">
								<input type="password" name="sign_id" required>
								<input type="submit" value="SEARCH">
							</div>
						</div>

					</div>

					<input type="hidden" name="_token" value="{{csrf_token()}}">

					
				</form>

				<div class="share_btn">
					<h4><br><br><br><br><br><br>
					</h4>
				</div>
			</div>
		</div>
	</div>

@endsection