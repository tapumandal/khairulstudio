
@extends('layouts.master')

@section('title', 'Download')

@section('navigation_bar')
	@include('include.cover')
@endsection

@section('main_content')
	
<div class="total_motion_p">

	<div class="download_p motion" >
		
		<script>

		</script>

		<div class="col-sm-1"></div>
		<div class="col-sm-10 public_motion">
			<div class="download_h ">
					<div class="d_h_1">
						<h1>MOTION</h1>	
					</div>

					<div class="d_h_2">
						<h3>DIRECTING WORK</h3>	
					</div>
			</div>

			<div class="container-fluid text-center bg-grey">
			  <div class="row text-center">

			    	@foreach($motionLink as $key => $motion)

			    			@if($motion->site == "youtube")
		                      	<div class="col-sm-4">
							       	<iframe   src='https://www.youtube.com/embed/{{ $motion->link }}?modestbranding=1&autohide=1&showinfo=0&controls=0"'></iframe>
							        <p><strong>{{$motion->title}}</strong></p>
						    	</div>
						    @elseif($motion->site == "vimeo")
						    	<div class="col-sm-4">
							       	<?php echo $motion->link;  ?>
							        <p><strong>{{$motion->title}}</strong></p>
						    	</div>
						    @endif

                    @endforeach
			  </div>
			</div>
		</div>
		<div class="col-sm-1"></div>


		<script>
			// $("iframe").contents().find(".ytp-chrome-top").css("z-index", "0");
		</script>
	</div>
</div>
@endsection