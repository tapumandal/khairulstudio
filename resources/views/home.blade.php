
@extends('layouts.master')

@section('title', 'Khairul Studio')

@section('navigation_bar')
	@include('include.cover')
@endsection

@section('main_content')
	
	<div class="col-sm-12 slider home_slider">
		<style>
		  .carousel-inner > .item > img, .carousel-inner > .item > a > img {
		      width: 70%;
		      margin: auto;
		  	}
		</style>
	  <div id="myCarousel" class="carousel slide" data-ride="carousel">
	    <!-- Indicators -->
			<ol class="carousel-indicators">
			  <li id="setActive" data-target="#myCarousel" data-slide-to="0"><div class="carousel-indicators-inside"></div></li>
			  <li data-target="#myCarousel" data-slide-to="1"><div class="carousel-indicators-inside"></div></li>
			  <li data-target="#myCarousel" data-slide-to="2"><div class="carousel-indicators-inside"></div></li>
			</ol>

			<div class="dextop_view_h_slide">
			    <div class="carousel-inner" role="listbox">
				      <div class="item active">
				        <img class="img-responsive" src="{{ URL::asset ('images/public/sh1.jpg') }}">
				      </div>

				      <div class="item">
				        <img class="img-responsive" src="{{ URL::asset ('images/public/sh2.jpg') }}">
				      </div>
				    
				      <div class="item">
				        <img class="img-responsive" src="{{ URL::asset ('images/public/sh3.jpg') }}">
				      </div>
			    </div>
			</div>

		    <div class="mobile_view_h_slide">
			    <div class="carousel-inner" role="listbox">
				      <div class="item active" style="">
				        <img class="img-responsive" >
				      </div>

				      <div class="item">
				        <img class="img-responsive">
				      </div>
				    
				      <div class="item">
				        <img class="img-responsive">
				      </div>
			    </div>
			</div>



	  </div>
		<script type="text/javascript">
				
				setTimeout(myFunction, 50);
				function myFunction() {
				    document.getElementById("setActive").className = "active";
				}
		</script>
	</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>

	<div class="col-sm-12 home_box">
			
		<!-- <div class="col-sm-6"> -->
			<a href="lifestyle">
				<div class="single_box">
					
					<div class="single_box_text">
						<div class="box_title"> <h3>Life Style</h3></div>	
						<div class="box_disc ml11">
							<!-- <span class="line line1"></span> -->
												<span class="letters">Here you will be shown with a highly evocative look! It will help you find out the way you like yourself. 
											  So check it and deal with own, what you want? </span>
											</div>
					</div>
					<img class="img-responsive" src="{{ URL::asset ('images/public/sci1.jpg') }}">
				</div>
			</a>
			<a href="wedding">
				<div class="single_box">
					<div class="single_box_text">
						<div class="box_title"> <h3>Wedding</h3></div>	
						<div class="box_disc ml12"><span class="letters">Your precious moments will be picked up
 with photos. 
it will describe your weedding story.
 So check it out and contact us for booking. </span></div>
					</div>
					<img class="img-responsive" src="{{ URL::asset ('images/public/sci2.jpg') }}">
				</div>
			</a>
		<!-- </div> -->
		<a href="familyportrait">
		<!-- <div class="col-sm-6"> -->											
				<div class="single_box">
					<div class="single_box_text">
						<div class="box_title"> <h3>Family Portrait</h3></div>	
						<div class="box_disc ml13"><span class="letters">This is one of the most important things in your home.
it's great for your family to have permanent memories of your family for a specific time.
So check it out and contact us. </span></div>
					</div>
					<img class="img-responsive" src="{{ URL::asset ('images/public/sci3.jpg') }}">
				</div>
			</a>
			<a href="studio">
				<div class="single_box">
					<div class="single_box_text">
						<div class="box_title"> <h3>Studio</h3></div>	
						<div class="box_disc ml14"><span class="letters">Check it and deal with own, what you want?</span></div>
					</div>
					<img class="img-responsive" src="{{ URL::asset ('images/public/sci4.jpg') }}">
				</div>
			</a>
		<!-- </div> -->
	</div>
@endsection