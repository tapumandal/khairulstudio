
@extends('layouts.master')

@section('title', 'Gallary')

@section('navigation_bar')
	@include('include.cover')
@endsection

@section('main_content')
	
	
	<style type="text/css">
		@media screen and (min-width: 768px){
		
			html{
				overflow: hidden;
			}
			.col-sm-12.slider.store_slider{
				overflow-y: hidden;
			}
		}
	</style>

	<div class="col-sm-12 slider store_slider">
		<div class="slider_inside">
				@foreach($imageList as $image)
					<img class="img-responsive" src="{{ URL::asset ('images/gallary/'.$image->img_name.'') }}">
				@endforeach
				
		</div>
		
		


		<script type="text/javascript">
				
				

				var windowWidthIn = $( window ).width();
				
				if(windowWidthIn>500){
					setTimeout(countSlider, 3000);
				}

				function countSlider(){
					var count = $(".slider_inside img").length;
					
					var imgWidth=0;
					var i = 0;

					for (i = 1; i <= count; i++) { 
					    y = $(".slider_inside img:nth-child("+i+")").width();
					    // alert(y);
					    imgWidth = imgWidth + y+13;
					    // alert(i+" > "+y);
					}

					$(".slider_inside").css("cssText", "width:"+imgWidth+"px;");			
				}
		</script>
	</div>
@endsection