@extends('layouts.master')

@section('title', 'Shop')

@section('navigation_bar')
	@include('include.cover')
@endsection

@section('main_content')
	

	<div class="shop_title">
				<h3>Shop now with great discount!</h3>
	</div>

	<div class="download_p">
		
		<div class="col-sm-1"></div>
		<div class="col-sm-10 public_shop">
			
			<div class="container-fluid text-center bg-grey">
			  <div class="row text-center">

			    	@foreach($products as $key => $product)
			    		<a href="{{$product->webaddress}}{{$product->campaign}}{{$product->promoid}}" target="_blink">
	                      	<div class="col-sm-3">
						      <div class="thumbnail">
						      	<div class="shop_img">
						        	<img class="img-responsive" src="images/shop/{{$product->img}}" alt="{{$product->name}}">
						        </div>
						        <p><strong>{{$product->name}}</strong></p>
						        <p>{{$product->des}}</p>
						      </div>
					    	</div>
					    </a>
                    @endforeach
			  </div>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>

@endsection