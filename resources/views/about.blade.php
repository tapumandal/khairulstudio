
@extends('layouts.master')

@section('title', 'About')

@section('navigation_bar')
	@include('include.cover')
@endsection

@section('main_content')
	
	<div class="col-md-12 contact about">
		<div class="contact_inside"">
			<div class="about_text">
				<span>Hi, I am Khairul Ahmed<br>
					 the commercial photographer. If you are interested<br>
					 in lifestyle & family portrait and want to wed or commercial<br>
					 product photography then you are in the right place.<br>
	 			</span>
			</div>
		</div>
	</div>
@endsection