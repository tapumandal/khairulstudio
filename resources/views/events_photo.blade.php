
@extends('layouts.master')

@section('title', 'Download')

@section('navigation_bar')
	@include('include.cover')
@endsection

@section('main_content')
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script type="text/javascript">
		$( function() {
	    	$( "#datepicker" ).datepicker();
	  	} );
	</script>
	<div class="total_download_p">
		<div class="download_p">
			
			<div class="download_h">
				<div class="d_h_1">
					<h1>DOWNLOAD</h1>	
				</div>
			</div>
			
			<div class="download_line"></div>
			<div class="download_panel col-sm-12">
				

				@foreach($eventList as $event)
					<div class="col-sm-4">
						<div class="event_info">
							<div class="event_info_left">
									<span>Photo & Video Type: </span>
									<span>Date of Shoot: </span>
									<span>Location: </span>
									<span>Sign ID: </span>	
							</div>

							<div class="event_info_right">
									<span>{{ $event->event_type }}</span>
									<span>{{ $event->event_date }}</span>
									<span>{{ $event->event_note }}</span>
									<span>{{ Crypt::decryptString($event->sign_id) }}</span>	
							</div>

							
						</div>
						<div class="event_download_btn">
							<form action="downloading" method="post" target="_blank">
								<input type="hidden" value="{!! csrf_token() !!}" name="_token">
								<input type="hidden" name="event_code" value="{{ $event->event_code }}">
								<input type="hidden" name="title" value="{{ $event->event_title }}">

								<input type="submit" value="Download">
							</form>
						</div>
					</div>
				@endforeach



				<div class="share_btn">
					<h4><br><br><br><br><br><br>
					</h4>
				</div>
			</div>
		</div>
	</div>

@endsection