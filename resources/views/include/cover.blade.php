
<div class="overlay_nav">
	<a href="./fashion">Fashion</a> 
	<a href="./lifestyle">Life Style</a> 
    <a href="./wedding">Wedding</a> 
    <a href="./familyportrait">Family Portrait</a> 
    <a href="./studio">Studio</a> 
    <a href="./motion">Motion</a> 
    <a href="./shop">Shop</a> 
    <a href="./lifestyle">Recent</a> 
</div>


<div class="col-sm-12   cover " >
	<div class="col-sm-12 cover_inside">
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
			<div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" onclick="showNav()">
		        <span class="icon-bar"></span>
		        <!-- <span class="icon-bar"></span> -->
		        <span class="icon-bar"></span>                        
		      </button>
		    </div>
		  </div>
		</nav>
			
		<div class="logo">
			<a href="./"> <img class="img-responsive" src="{{ URL::asset('images/default/logo_khairulstudio.png') }}" alt="Khairul Studio Logo"> </a>
		</div>



	



		<div class="header_btn">



			

			<div class="menu_btn">
				<div class="home_option">
					<a class="option_tag" onclick="showDropDown()" href="#"> <img class="img-responsive" src="{{ URL::asset('images/default/menu-icon2.png') }}"> </a>

					<div class="dropdown">
					  <!-- <button class="dropbtn"></button> -->
					  <div class="dropdown-content">
					  
					  	<div class="arrow-down"></div>
					  	<div class="drop_menu_top">
					  		<span>Your photo is missing</span>
					  	</div>
					  	<div class="drop_down_underline"></div>

					  	<div class="drop_menu_top_option">
						    <a href="./download"><img class="img-responsive" src="{{ URL::asset('images/icon/download.png') }}"> Download</a>
						    <a href="./contact"><img class="img-responsive" src="{{ URL::asset('images/icon/contact.png') }}">Contact</a>
	    					<a href="./about"><img class="img-responsive" src="{{ URL::asset('images/icon/about.png') }}">About</a>
	    				</div>
    					<div class="drop_menu_top_dot">
    						<i class="fa fa-circle" aria-hidden="true"></i>	
    						<i class="fa fa-circle" aria-hidden="true"></i>	
    						<i class="fa fa-circle" aria-hidden="true"></i>	
    					</div>
    					<div class="drop_menu_top_social">
    						<a href=""> <i class="fa fa-facebook-square" aria-hidden="true"></i> </a>
    						<a href=""> <i class="fa fa-instagram" aria-hidden="true"></i> </a>
    						<a href=""> <i class="fa fa-flickr" aria-hidden="true"></i> </a>
    						<a href=""> <i class="fa fa-pinterest-square" aria-hidden="true"></i> </a>
    						<a href=""> <i class="fa fa-youtube-play" aria-hidden="true"></i> </a>
    					</div>
    					
		                  <!-- @if(Auth::check())
	                            <a href="{{ route('logout') }}"
	                                onclick="event.preventDefault();
	                                         document.getElementById('logout-form').submit();">
	                                Logout
	                            </a>

	                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                {{ csrf_field() }}
	                            </form>
	                        @else
	                        <a href="{{ route('login') }}">Login</a>
	                        <a href="{{ route('register') }}">Register</a>
	                      @endif -->
	                      
					  </div>
					</div>
				</div>
			</div>

			<div class="wide_screen_nav">
				<nav class="navbar navbar-inverse">
				  <div class="container-fluid">

				    <div class="collapse navbar-collapse" id="myNavbar" style="display: none;">
				      <ul class="nav navbar-nav">
				      	
				      	<li>
					      	<div class="hoverdown">
							    <a class="top_nav_ul" class="nav_underline">Portfolio</a>
							    <i class="fa fa-caret-down"></i>
							    <ul class="hoverdown-content">
							      	<li><a class="portfolio_a" href="./fashion">Fashion</a>  </li>
							      	<li><a class="portfolio_a" href="./lifestyle">Life Style</a>  </li>
							        <li><a class="portfolio_a" href="./wedding">Wedding</a>  </li>
							        <li><a class="portfolio_a" href="./familyportrait">Family Portrait</a>  </li>
							        <li><a class="portfolio_a" href="./studio">Studio</a>  </li>
							    </ul>
							</div>
						</li>
				        
				        <li><a class="top_nav_ul" href="./motion">Motion</a>  <div class="nav_underline"></div>  </li>
				        <li><a class="top_nav_ul" href="./shop">Shop</a>  <div class="nav_underline"></div>  </li>
				        <li><a class="top_nav_ul" href="./lifestyle">Recent</a>  <div class="nav_underline"></div>  </li>
				        
				      </ul>
				    </div>
				  </div>
				</nav>

			</div>
		</div>
		
	</div>
</div>

	
	