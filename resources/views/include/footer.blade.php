	
	
	
	@if(Request::path() === 'fashion' ||Request::path() === 'lifestyle' || Request::path() === 'wedding' || Request::path() === 'familyportrait' || Request::path() === 'studio')
			<?php
				$dynamicFooter="dynamicFooter";
	
			?>
	@elseif(Request::path() === '/')
			<?php
				$dynamicFooter="homeDFooter";
			?>
	@elseif(Request::path() == 'motion' || Request::path() == 'shop')
			<?php
				$dynamicFooter="";
			?>
	@else
			<?php
				$dynamicFooter="dF";
			?>
	@endif
	
	
<div class="footer {{ $dynamicFooter }}">
	<div class="footer_inside ">
		<div class="footer_option">
			<div class="col-sm-12">
					<div class="col-sm-2" style="max-width: 160px;">
						<h5>Shop</h5>

						<div class="footer_option_list">
							<ul>
								<li> <a target="_black" href="http://teespring.com/stores/tbox"> Apparel </a> </li>
								<li> <a target="_black" href="http://teespring.com/stores/beachtowel"> Beach Towel </a> </li>
								<li> <a target="_black" href="http://teespring.com/stores/indoorpillow"> Indoor Pillow </a> </li>
								<li> <a target="_black" href="http://teespring.com/stores/walltapestry"> Wall Tapestry </a> </li>
								<li> <a target="_black" href="http://teespring.com/stores/iphonebox"> iPhone case </a> </li>
								<li> <a target="_black" href="http://teespring.com/stores/totebags"> Tote Bag </a> </li>
								<li> <a target="_black" href="http://teespring.com/stores/socksx"> Socks </a> </li>
								<li> <a target="_black" href="http://teespring.com/stores/amug"> Mug </a> </li>
							</ul>
						</div>
					</div>	

					<div class="col-sm-2">
						<h5>About Khairul Studio</h5>

						<div class="footer_option_list">
							<ul>
								<li>Job Opportunities</li>
								<li>Contact</li>
								<li>Events</li>
							</ul>
						</div>
					</div>
			</div>
		</div>

		<div class="footer_real">
			<div class="footer_top">
				<span>More ways to shop: Visit an <a href="shop" style="color:#0070c9"> Khairul Studio Store</a>, call +880 1783 611431</span>
			</div>
			<hr>
			<div class="footer_bottom">
				<span>Copyright © 2017 <a href=".">khairul studio</a>. All rights reserved.&emsp;&emsp;</span>
				<span>Developed by <a target="_blank" href="http://www.aaoaz.com/email.php">aaoaztech</a></span>
				<span>Bangladesh</span>
			</div>
		</div>
	</div>
</div>