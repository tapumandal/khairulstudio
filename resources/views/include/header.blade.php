<!DOCTYPE html>
<html>
<head>
	<title>Khairul Studio</title>
	<link rel="shortcut icon" href="http://www.khairulstudio.com/images/icon/tab_l.ico" type="image/ico" />
<!-- bootstrap -->
	<!-- <script src="bootstrap/js/bootstrap.min.js"></script> -->
	<link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet" />
	<script src="{{ URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js') }}"></script>
	<!-- bootstrap -->

	<!-- for nav bar -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') }}">
    <link href="{{ URL::asset('css/navbar.css') }}" rel="stylesheet" />
    <script src="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') }}"></script>
    <!-- for nav bar -->

	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('css/bootstrapModified.css') }}" rel="stylesheet" />


	<script src="{{ URL::asset('js/self.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/slider.js') }}"></script>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	

</head>
<body>

<div class="container-full">
	


