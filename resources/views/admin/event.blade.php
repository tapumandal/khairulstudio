@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Events</div>
                <div class="panel-body">
                    
                    <div class="col-md-3"></div>
                    <div class="col-md-6 contact_form">

                            @if (session('actionStatus'))
                              <div class="alert alert-success " style="width: 90%;">
                                  {!! session('actionStatus') !!}
                              </div>
                            @endif

                        <h3 style="text-align: left; width: 100%;">Create New Event</h3>

                        <form id="html5Form" action="{{ url('createevent') }}/{{ $clientid }}" method="post">
                          

                          <div class="input-group">
                            <input id="msg" type="hidden" class="form-control" name="sign_id" value="{{ $sign_id }}">
                          </div>

                          <div class="input-group">
                            <span class="input-group-addon">Event Title</span>
                            <input id="msg" type="text" class="form-control" name="event_title" maxlength="30" minlength="4">
                          </div>

                          <div class="input-group">
                            <span class="input-group-addon">Event Date</span>
                            <input id="msg" type="date" class="form-control" name="event_date">
                          </div>

                          <div class="input-group">
                            <span class="input-group-addon">Event Type</span>
                            <input id="msg" type="text" class="form-control" name="event_type" minlength="3">
                          </div>

                          <div class="input-group">
                            <span class="input-group-addon">Location</span>
                            <input id="msg" type="text" class="form-control" name="event_note" minlength="8">
                          </div>

                          <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                          <div class="input-group" style="margin-top: 10px;">
                            <input id="msg" type="submit" class="form-control btn btn-default" value="Create">
                          </div>
                        </form>                        
                    </div>

                    <div class="col-md-6">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 ">
            <div class="panel panel-default">
                <!-- <div class="panel-heading"><h3>Events</h3></div> -->
                    <div class="panel-body event">


                        @foreach($events as $event)
                            <div class="col-md-4">
                                <h4>{!! $event->event_title !!}</h4>
                                <div class="event_info">
                                    <span><b> {!! $event->name !!} </b></span>
                                    <h4>Event Code: {!! $event->event_code !!}</h4>
                                    {{-- <span> <b>Title: </b> {!! $event->event_title !!}   </span> --}}
                                    <span> <b>Location: </b> {!! $event->event_note !!} </span>
                                    <span> <b>Date: </b> {!! $event->event_date !!}   </span>
                                </div> 

                                <form id="html5Form" action="{{ url('imgselect') }}" method="post">
                                    <input type="hidden" name="event_code" value="{!! $event->event_code!!}">
                                    <input type="hidden" name="name" value="{!! $event->name!!}">
                                    <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                                    <input id="msg" type="submit" class="form-control btn btn-default" value="Upload Image">
                                </form> 

                                <form id="html5Form" action="{{ url('display') }}" method="post">
                                    <input type="hidden" name="event_code" value="{!! $event->event_code!!}">
                                    <input type="hidden" name="name" value="{!! $event->name!!}">
                                    <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                                    <input id="msg" type="submit" class="form-control btn btn-default" value="Show Image">
                                </form>

                                <form id="html5Form" action="{{ url('deactivate') }}" method="post">
                                    <input type="hidden" name="event_code" value="{!! $event->event_code !!}">
                                    <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                                    <input id="msg" type="submit" class="form-control btn btn-default" value="Deactivate">
                                </form>



                                {{-- <form id="html5Form" action="{!!action('AdminUserTask@email')!!}" method="post">
                                    <input type="hidden" name="event_code" value="{!! $event->event_code!!}">
                                    <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                                    <input id="msg" type="submit" class="form-control btn btn-default" value="Email Event Code">
                                </form>    --}}  
                            </div>
                        @endforeach

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection










