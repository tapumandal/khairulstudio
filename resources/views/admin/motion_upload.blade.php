@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                    <h2 style="text-align: center">Motion</h2>
                <div class="panel-body">

                    <div class="col-md-3"></div>
                    <div class="col-md-6 contact_form">
                        <form id="html5Form" action="upmotion" method="post">
                          
                          <div class="input-group">
                            <span class="input-group-addon">Youotube Link</span>
                            <input id="msg" type="hidden" class="form-control" name="site[]" value="youtube">
                            <input id="msg" type="text" class="form-control" name="motionLink[]" placeholder="Link">
                            <input id="msg" type="text" class="form-control" name="linkTitle[]" placeholder="Title">
                          </div>
                          <div class="input-group">
                            <span class="input-group-addon">Youotube Link</span>
                            <input id="msg" type="hidden" class="form-control" name="site[]" value="youtube">
                            <input id="msg" type="text" class="form-control" name="motionLink[]" placeholder="Link">
                            <input id="msg" type="text" class="form-control" name="linkTitle[]" placeholder="Title">
                          </div>

                          <div class="input-group">
                            <span class="input-group-addon">Motion Embed</span>
                            <input id="msg" type="hidden" class="form-control" name="site[]" value="vimeo">
                            <input id="msg" type="text" class="form-control" name="motionLink[]" placeholder="Link">
                            <input id="msg" type="text" class="form-control" name="linkTitle[]" placeholder="Title">
                          </div>
                          <div class="input-group">
                            <span class="input-group-addon">Motion Embed</span>
                            <input id="msg" type="hidden" class="form-control" name="site[]" value="vimeo">
                            <input id="msg" type="text" class="form-control" name="motionLink[]" placeholder="Link">
                            <input id="msg" type="text" class="form-control" name="linkTitle[]" placeholder="Title">
                          </div>

                          <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                          <div class="input-group" style="margin-top: 10px;">
                            <input id="msg" type="submit" class="form-control btn btn-default" value="Upload">
                          </div>
                        </form>

                    </div>

                </div>

                <div class="panel-body motion_view">

                    @foreach($motionLink as $link => $value)

                      @if($value->site == "youtube")
                            <div class="col-sm-4">
                                  <iframe   src='https://www.youtube.com/embed/{{ $value->link }}?modestbranding=1&autohide=1&showinfo=0&controls=0"'></iframe>
                                  <p><strong>{{$value->title}}</strong></p>
                              


                                <form id="html5Form" action="demotion" method="post">
                                  
                                    <input type="hidden" class="form-control" name="videoLink" value="{{$value->link}}">
                                  

                                  <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                                  <div class="input-group" style="margin-top: 10px;">
                                    <input id="msg" type="submit" class="form-control btn btn-default" value="Remove">
                                  </div>
                                </form>
                            </div>

                        @elseif($value->site == "vimeo")
                            <div class="col-sm-4">
                                <?php echo $value->link;  ?>
                                <p><strong>{{$value->title}}</strong></p>
                                <form id="html5Form" action="demotion" method="post">
                                  
                                    <input type="hidden" class="form-control" name="videoLink" value="{{$value->link}}">
                                  

                                  <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                                  <div class="input-group" style="margin-top: 10px;">
                                    <input id="msg" type="submit" class="form-control btn btn-default" value="Remove">
                                  </div>
                                </form>
                            </div>

                        @endif


                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
