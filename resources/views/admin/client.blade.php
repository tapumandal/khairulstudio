@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Client</div>
                <div class="panel-body">
                    
                    <div class="col-md-3"></div>
                    <div class="col-md-6 contact_form">

                            @if (session('actionStatus'))
                              <div class="alert alert-success " style="width: 90%;">
                                  {!! session('actionStatus') !!}
                              </div>
                            @endif

                        {{-- <h3 style="text-align: left; width: 100%;">New Client</h3> --}}

                        <form id="html5Form" action="createclient" method="post">
                          <div class="input-group">
                            <span class="input-group-addon">Client Name</span>
                            <input id="msg" type="text" class="form-control" name="client_name" maxlength="20" minlength="5" required>
                          </div>

                          <div class="input-group">
                            <span class="input-group-addon">Email</span>
                            <input id="email" type="email" class="form-control" name="email" maxlength="50" minlength="6" required="email">
                          </div>

                          <div class="input-group">
                            <span class="input-group-addon">Phone</span>
                            <input id="msg" type="text" class="form-control" name="phone" value="880" maxlength="14" minlength="7">
                          </div>

                          <div class="input-group">
                            <span class="input-group-addon">Location</span>
                            <input id="msg" type="text" class="form-control" name="location" value="" maxlength="100" minlength="5">
                          </div>



                          <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                          <div class="input-group" style="margin-top: 10px;">
                            <input id="msg" type="submit" class="form-control btn btn-default" value="Create">
                          </div>
                        </form>                        
                    </div>

                    <div class="col-md-6">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 ">
            <div class="panel panel-default">
                <!-- <div class="panel-heading"><h3>Events</h3></div> -->
                    <div class="panel-body event">


                        @foreach($clients as $client)
                            <div class="col-md-4">
                                <h4><b> {!! $client->name !!} </b> </h4>
                                <div class="event_info">
                                    <div class="col-sm-5">
                                        <span><b>Sign ID  </b></span>
                                        <span><b>Email  </b></span>
                                        <span><b>Phone  </b></span>
                                        <span><b>Location  </b></span>

                                    </div>

                                    <div class="col-sm-7">
                                          <span><b style="color: red;">{!! $client->sign_id !!}</b></span>
                                          <span>{!! $client->email !!}    </span>
                                          <span> {!! $client->phone !!}    </span>
                                          <span> {!! $client->event_date !!}   </span>
                                          <span> {!! $client->location !!} </span>  
                                    </div>
                                    
                                    
                                </div>

                                <a class="form-control btn btn-default" href="{{ url('event') }}/{{ $client->id }}">Event</a>

                                
                            </div>
                        @endforeach

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection










