@extends('layouts.admin')

@section('content')
<div class="container">
	



    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Image Display</div>
                <div class="panel-body">

					<div class="gridLayout">
						<div class="gridLayoutInside">
							<div class="container-fluid">
							  <div class="row">
							  	 <?php
                                	if($type =='gallary'){
                                		echo '
							                <div class="display_all_img col-sm-12">
					                          <div class="col-md-2">
					                            <a href="./fashion">Display Fashion Image</a>
					                          </div>

					                          <div class="col-md-2">
					                            <a href="./lifestyle">Display Life Style Image</a>
					                          </div>

					                          <div class="col-md-2">
					                            <a href="./wedding">Display Wedding Image</a>
					                          </div>

					                          <div class="col-md-2">
					                            <a href="./familyportrait">Display Family Portrait Image</a>
					                          </div>

					                          <div class="col-md-2">
					                            <a href="./studio">Display Studio Image</a>
					                          </div>
					                        </div>
							                ';
                                	}
                                	else{
                                		$imgUrl = "images/users/";	
                                	}
                                ?>


							  	<form action="{{ action('AdminAction@deleteImage') }}"  method="post">
							  		
							  		<input type="hidden" name="event_code" value="{!! $event_code!!}">
							  		<input type="hidden" name="cata" value="{!! $cata!!}">
                                    <input type="hidden" name="type" value="{!! $type!!}">
                                    <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                                    
                                    
                                    <?php
                                    	if($type =='gallary'){
                                    		$imgUrl = "images/gallary/";
                                    	}
                                    	else{
                                    		$imgUrl = "images/users/";	
                                    	}
                                    ?>
                                    

								  	@foreach($imgList as $image)
								    <div class="imgGridBox">
								  		<div class="imgGrid">
								  			<input type="checkbox" name="images[]" value="{{ $image->img_name }}">
								  			<img class="img-responsive" src="{{ URL::asset(''.$imgUrl.''.$image->img_name.'') }}">

								  		</div>
								    </div>
								    @endforeach

								    <input id="msg" type="submit" class="form-control btn btn-default" value="Delete">
								</form>

							    
							  </div>
							</div>

						</div>

					</div>

				</div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
		$(window).load(function(){
		 $('.imgGrid').find('img').each(function(){
		  var imgClass = (this.width/this.height > 1) ? 'wide' : 'tall';

		  $(this).addClass(imgClass);
		 });
		});
	</script>

</div>
@endsection
