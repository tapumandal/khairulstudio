@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 shop_upload">
            <div class="panel panel-default">
                    @if (session('status'))
                        <div class="alert alert-success" style="text-align: center;">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h2 style="text-align: center">Shop</h2>
                <div class="panel-body">
                  
                  

                    <div class="col-md-3"></div>
                    <div class="col-md-6 contact_form">
                        <form id="html5Form" action="shopup" method="post" enctype="multipart/form-data">
                          <div class="input-group">
                            <span class="input-group-addon">Select Website</span>
                            <select class="form-control" name="webaddress">
                              <option value="http://teespring.com/">teespring</option>
                              <option value="http://amazon.com/">amazon</option>
                            </select>
                          </div>
                          <br>

                          <div class="input-group">
                            <span class="input-group-addon">Product 1</span>
                            <input style="opacity: 1;" type="file" class="form-control slider_img_btn" id="images" name="images[]" onchange="preview_images();" multiple/>
                            <input id="msg" type="text" class="form-control" name="campaign[]" placeholder="Your Campaign">
                            <input id="msg" type="text" class="form-control" name="promoid[]" placeholder="Promo Id">
                            <input id="msg" type="text" class="form-control" name="name[]" placeholder="Name">
                            <input id="msg" type="text" class="form-control" name="des[]" placeholder="Description (optional)">
                          </div>

                          <!-- <div class="input-group">
                            <span class="input-group-addon">product 2</span>
                            <input style="opacity: 1;" type="file" class="form-control slider_img_btn" id="images" name="images[]" onchange="preview_images();" multiple/>
                            <input id="msg" type="text" class="form-control" name="campaign[]" placeholder="Your Campaign">
                            <input id="msg" type="text" class="form-control" name="promoid[]" placeholder="Promo Id">
                            <input id="msg" type="text" class="form-control" name="name[]" placeholder="Name">
                            <input id="msg" type="text" class="form-control" name="des[]" placeholder="Description (optional)">
                          </div>

                          <div class="input-group">
                            <span class="input-group-addon">Product 3</span>
                            <input style="opacity: 1;" type="file" class="form-control slider_img_btn" id="images" name="images[]" onchange="preview_images();" multiple/>
                            <input id="msg" type="text" class="form-control" name="campaign[]" placeholder="Your Campaign">
                            <input id="msg" type="text" class="form-control" name="promoid[]" placeholder="Promo Id">
                            <input id="msg" type="text" class="form-control" name="name[]" placeholder="Name">
                            <input id="msg" type="text" class="form-control" name="des[]" placeholder="Description (optional)">
                          </div> -->

                          <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                          <div class="input-group" style="margin-top: 10px;">
                            <input id="msg" type="submit" class="form-control btn btn-default" value="Upload">
                          </div>
                        </form>

                    </div>

                    <div class="col-md-3">
                      <div class="preview_img">
                          <img class="img-responsive" id="pre_img_1" src="">
                          <img class="img-responsive" id="pre_img_2" src="">
                          <img class="img-responsive" id="pre_img_3" src="">
                      </div>
                    </div>

                </div>

                <div class="panel-body motion_view">

                    @foreach($products as $key => $product)
                      
                        <div class="col-sm-3">
                          <div class="thumbnail">
                            <div class="shop_img">
                              <img class="img-responsive" src="images/shop/{{$product->img}}" alt="{{$product->name}}">
                            </div>
                            <p><strong>{{$product->name}}</strong></p>
                            <p>{{$product->des}}</p>
                          

                            <form id="html5Form" action="shopremove" method="post">
                                <input type="hidden" class="form-control" name="campaign" value="{{$product->campaign}}">
                                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                                <div class="input-group" style="margin-top: 10px;">
                                  <input id="msg" type="submit" class="form-control btn btn-default" value="Remove">
                                </div>
                            </form>
                            
                          </div>

                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
