@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 gallary">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Gallary Image Uploading.</h3>
                  <span style="font-size:10px;">Do not upload more than 8MB</span>
                  
                  @if(isset($status))
                    <div class="alert alert-success " style="width: 90%; margin-top:10px;">
                        {!! $status; !!} 
                    </div>
                  @endif
                </div>


                <div class="panel-body">
                     <script>
                      function preview_images() 
                      {
                         var total_file=document.getElementById("images").files.length;
                         for(var i=0;i<total_file;i++)
                         {
                          $('#image_preview').append("<div class='col-md-2 single_img_preview'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
                         }
                      }
                      </script>
                      <div class="panel-body">
                        <div class="display_all_img col-sm-12">

                          <div class="col-md-2">
                            <a href="display/fashion">Display Fashion Image</a>
                          </div>

                          <div class="col-md-2">
                            <a href="display/lifestyle">Display Life Style Image</a>
                          </div>

                          <div class="col-md-2">
                            <a href="display/wedding">Display Wedding Image</a>
                          </div>

                          <div class="col-md-2">
                            <a href="display/familyportrait">Display Family Portrait Image</a>
                          </div>

                          <div class="col-md-2">
                            <a href="display/studio">Display Studio Image</a>
                          </div>
                        </div>
                         <div class="row">
                             <form action="uploadgallaryimg" method="post" enctype="multipart/form-data">
                              <div class="col-md-2"></div>
                              <div class="col-md-5">
                                    <select name="catagory">
                                        <option value="fashion">Fashion</option>
                                        <option value="lifestyle">Life Style</option>
                                        <option value="wedding">Wedding</option>
                                        <option value="familyportrait">Family Portrait</option>
                                        <option value="studio">Studio</option>
                                    </select>

                                    <input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple/>
                                    <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                              </div>
                              <div class="col-md-2">
                                  <input type="submit" class="btn btn-primary" name='submit_image' value="Upload Multiple Image"/>
                              </div>
                             </form>
                        </div>

                      <div class="row" id="image_preview"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
