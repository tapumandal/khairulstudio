@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Event Image Upload for {!! $name !!}. <br> Event Code is {!! $event_code !!}
                  <br><span style="font-size:10px;">Do not upload more than 8MB</span>
                  
                  @if(isset($status))
                    <div class="alert alert-success " style="width: 90%; margin-top:10px;">
                        {!! $status; !!} 
                    </div>
                  @endif
                </div>

                <div class="panel-body">
                      <script>
                      function preview_images() 
                      {
                         var total_file=document.getElementById("images").files.length;
                         for(var i=0;i<total_file;i++)
                         {
                          $('#image_preview').append("<div class='col-md-2 single_img_preview'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
                         }
                      }
                      </script>

                      <div class="panel-body">

                        <!-- <form action="{{ URL::to('sliderimg') }}" method="post" enctype="multipart/form-data">
                            <div class="img_row">
                                <div class="image_up_btn">
                                    <img class="img_select" src="{{ URL::asset('images/default/select.png') }}">
                                    <input id="input_img" class="slider_img_btn" type="file" name="up_img_1">
                                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                </div>
                            </div>
                            <input class="slider_img_up" type="submit" value="UPLOAD">

                       </form> -->


                          <div class="row">
                             <form action="imgupload" method="post" enctype="multipart/form-data">
                              <div class="col-md-6">
                                    <input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple/>
                                    <input type="hidden" name="event_code" value="{!! $event_code !!}">
                                    <input type="hidden" name="name" value="{!! $name !!}">
                                    <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                              </div>
                              <div class="col-md-6">
                                  <input type="submit" class="btn btn-primary" name='submit_image' value="Upload Multiple Image"/>
                              </div>
                             </form>
                          </div>
                          <div class="row" id="image_preview"></div>

                    </div>      
                </div>
            </div>
        </div>
    </div>
</div>
@endsection










