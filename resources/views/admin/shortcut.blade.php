@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 shortcut_img_up_panel">
            <div class="panel panel-default">
                <div class="panel-heading">Shortcut Image Upload
                    @if (session('actionStatus'))
                        <div class="alert alert-success">
                            {!! session('actionStatus') !!}
                        </div>
                    @endif
                 </div>
                <div class="panel-body">

                    <form action="{{ URL::to('shortcutimg') }}" method="post" enctype="multipart/form-data">
                        <div class="img_row">
                            <span>First Image</span>
                            <div class="image_up_btn">
                                <img class="img_selected" src="{{ URL::asset('images/default/selected.png') }}">
                                <img class="img_select" src="{{ URL::asset('images/default/select.png') }}">
                                <input id="input_img" class="shortcut_img_btn" type="file" name="up_img_1">
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            </div>
                        </div>


                        <div class="img_row">
                            <span>Second Image</span>
                            <div class="image_up_btn">
                                <img class="img_selected" src="{{ URL::asset('images/default/selected.png') }}">
                                <img class="img_select" src="{{ URL::asset('images/default/select.png') }}">
                                <input id="input_img" class="shortcut_img_btn" type="file" name="up_img_2">
                            </div>
                        </div>


                        <div class="img_row">
                            <span>Third Image</span>
                            <div class="image_up_btn">
                                <img class="img_selected" src="{{ URL::asset('images/default/selected.png') }}">
                                <img class="img_select" src="{{ URL::asset('images/default/select.png') }}">
                                <input id="input_img" class="shortcut_img_btn" type="file" name="up_img_3">
                            </div>
                        </div>

                         <div class="img_row">
                            <span>Third Image</span>
                            <div class="image_up_btn">
                                <img class="img_selected" src="{{ URL::asset('images/default/selected.png') }}">
                                <img class="img_select" src="{{ URL::asset('images/default/select.png') }}">
                                <input id="input_img" class="shortcut_img_btn" type="file" name="up_img_4">
                            </div>
                        </div>

                        <input class="slider_img_up" type="submit" value="UPLOAD">

                   </form>
                </div>

                <div class="preview_img">
                    <img class="img-responsive" id="pre_img_1" src="">
                    <img class="img-responsive" id="pre_img_2" src="">
                    <img class="img-responsive" id="pre_img_3" src="">
                    <img class="img-responsive" id="pre_img_4" src="">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- 
{{ URL::asset ('images/public/5.jpg') }}
{{ URL::asset ('images/public/5.jpg') }}
{{ URL::asset ('images/public/5.jpg') }} -->